﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkenJaData
{
    //static class Program
    //{
    //    /// <summary>
    //    /// The main entry point for the application.
    //    /// </summary>
    //    [STAThread]
    //    static void Main()
    //    {
    //        Application.EnableVisualStyles();
    //        Application.SetCompatibleTextRenderingDefault(false);
    //        Application.Run(new Form1());
    //    }
    //}

    public partial class Kategooriad : Form
    {
        public Kategooriad()
        {
            InitializeComponent();        
        }
        private void Kategooriad_Load(object sender, EventArgs e)
        {
            northwindEntities ne = new northwindEntities();
            this.dataGridView1.DataSource = ne
                .Categories
                .Select(x => new { x.CategoryID, x.CategoryName })
                .ToList();
        }

    }
}
